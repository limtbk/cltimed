import socket
import datetime


s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
s.connect((socket.gethostname(), 32777))
s.send(bytes("TIME","utf-8"))
ts = int(s.recv(16).decode("utf-8"), 16)
dt = datetime.datetime.fromtimestamp(ts)
print(dt)
s.close()