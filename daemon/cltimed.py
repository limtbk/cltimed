import socket
import serial
import serial.tools.list_ports as port_list
import datetime
import math

ports = list(port_list.comports())
port_path = ""
for port in ports:
    if "USB to UART" in port.description:
        port_path = port.device

baudrate = 115200
serialPort = serial.Serial(port=port_path, baudrate=baudrate, bytesize=8, timeout=1, stopbits=serial.STOPBITS_ONE)
serialString = ""
serialPort.write("V".encode())
s = serialPort.readline().decode('UTF-8')
if not("01.01" in s):
    print("No USB device found")
    exit()

def readUTCUnixtime():
    serialPort.write("T".encode())
    s = serialPort.readline().decode('UTF-8')
    unixtime = int(s, 16)
    return unixtime

def readCLUnixtime():
    serialPort.write("U".encode())
    s = serialPort.readline().decode('UTF-8')
    unixtime = int(s, 16)
    return unixtime

def readZeroPoint():
    serialPort.write("X".encode())
    s = serialPort.readline().decode('UTF-8')
    unixtime = int(s, 16)
    return unixtime

def convertUTCToCL(utctime):
    time = 'I{:016x}'.format(utctime)
    serialPort.write(time.encode())
    s = serialPort.readline().decode('UTF-8')
    unixtime = int(s, 16)
    return unixtime

def convertCLToUTC(utctime):
    time = 'J{:016x}'.format(utctime)
    serialPort.write(time.encode())
    s = serialPort.readline().decode('UTF-8')
    unixtime = int(s, 16)
    return unixtime

def setUTCTime(utctime):
    time = 'S{:016x}'.format(utctime)
    serialPort.write(time.encode())

def setCLTime(utctime):
    time = 'Q{:016x}'.format(utctime)
    serialPort.write(time.encode())

def setZeroPoint(utctime):
    time = 'Z{:016x}'.format(utctime)
    serialPort.write(time.encode())

def syncWithLocalClock():
    time = math.floor(datetime.datetime.now().timestamp())
    setUTCTime(time)

def readTimeCorrection():
    serialPort.write("Y".encode())
    s = serialPort.readline().decode('UTF-8')
    timeCorrection = int(s, 16)
    return timeCorrection

def setTimeCorrection(tc):
    tcs = 'w{:02x}'.format(tc)
    serialPort.write(tcs.encode())

s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
s.bind((socket.gethostname(), 32777))
s.listen(5)

while True:
    clientsocket, address = s.accept()

    msg = clientsocket.recv(4).decode("utf-8").upper()

    print(msg)
    if msg == "TIME":
        time = '{:016x}'.format(readUTCUnixtime())
        clientsocket.send(bytes(time,"utf-8"))

    if msg == "TIMC":
        time = '{:016x}'.format(readCLUnixtime())
        clientsocket.send(bytes(time,"utf-8"))

    if msg == "SETU": #SETUxxxxxxxxxxxxxxxx - set UTC time
        time = int(clientsocket.recv(16).decode("utf-8").upper(), 16)
        setUTCTime(time)

    if msg == "SETC": #SETCxxxxxxxxxxxxxxxx - set CL time
        time = int(clientsocket.recv(16).decode("utf-8").upper(), 16)
        setCLTime(time)

    if msg == "STCO": #STCOxx - set time correction
        tc = int(clientsocket.recv(2).decode("utf-8").upper(), 16)
        setTimeCorrection(tc)

    clientsocket.close()

serialPort.close()

# to run it as daemon use https://code-maven.com/convert-to-linux-service
