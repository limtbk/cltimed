/*
 * ds3231.c
 *
 *  Created on: Jun 4, 2023
 *      Author: lim
 */

#include "ds3231.h"

I2C_HandleTypeDef *phi2c;


// Convert normal decimal numbers to binary coded decimal
uint8_t decToBcd(uint8_t val)
{
  return (uint8_t)( (val/10*16) + (val%10) );
}
// Convert binary coded decimal to normal decimal numbers
uint8_t bcdToDec(uint8_t val)
{
  return (int)( (val/16*10) + (val%16) );
}

void Ds3231_Init(I2C_HandleTypeDef *hi2c)
{
	phi2c = hi2c;
}

void Ds3231_Set_Time(const struct tm *time)
{
	uint8_t set_time[7];
	set_time[0] = decToBcd(time->tm_sec);
	set_time[1] = decToBcd(time->tm_min);
	set_time[2] = decToBcd(time->tm_hour);
	set_time[3] = decToBcd(time->tm_wday);
	set_time[4] = decToBcd(time->tm_mday);
	set_time[5] = decToBcd(time->tm_mon + 1);
	set_time[6] = decToBcd(time->tm_year + 1900 - 2000);

	HAL_I2C_Mem_Write(phi2c, DS3231_ADDRESS, 0x00, 1, set_time, 7, 1000);
}

void Ds3231_Get_Time(struct tm *time)
{
	uint8_t get_time[7];
	HAL_I2C_Mem_Read(phi2c, DS3231_ADDRESS, 0x00, 1, get_time, 7, 1000);
	time->tm_sec = bcdToDec(get_time[0]);
	time->tm_min = bcdToDec(get_time[1]);
	time->tm_hour = bcdToDec(get_time[2]);
	time->tm_wday = bcdToDec(get_time[3]);
	time->tm_mday = bcdToDec(get_time[4]);
	time->tm_mon = bcdToDec(get_time[5]) - 1;
	time->tm_year = bcdToDec(get_time[6]) + 2000 - 1900;
}

void Ds3231_Set_TimeCorrection(int8_t timeCorrection)
{
	HAL_I2C_Mem_Write(phi2c, DS3231_ADDRESS, 0x10, 1, &timeCorrection, 1, 1000);
}

int8_t Ds3231_Get_TimeCorrection()
{
	int8_t timeCorrection;
	HAL_I2C_Mem_Read(phi2c, DS3231_ADDRESS, 0x10, 1, &timeCorrection, 1, 1000);
	return timeCorrection;
}
