/*
 * ds3231.h
 *
 *  Created on: Jun 4, 2023
 *      Author: lim
 */

#ifndef SRC_DS3231_H_
#define SRC_DS3231_H_

#include "stm32f1xx_hal.h"
#include "time.h"
//#include "stdint.h"

#define DS3231_ADDRESS 0xD0

void Ds3231_Init(I2C_HandleTypeDef *hi2c);
void Ds3231_Set_Time(const struct tm *time);
void Ds3231_Get_Time(struct tm *time);
void Ds3231_Set_TimeCorrection(int8_t corr);
int8_t Ds3231_Get_TimeCorrection();

#endif /* SRC_DS3231_H_ */
