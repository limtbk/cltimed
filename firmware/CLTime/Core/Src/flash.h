/*
 * flash.h
 *
 *  Created on: Jun 7, 2023
 *      Author: lim
 */

#ifndef SRC_FLASH_H_
#define SRC_FLASH_H_

#include "stm32f1xx_hal.h"

uint32_t Flash_Write_Data (uint32_t StartPageAddress, uint32_t *Data, uint16_t numberofwords);
void Flash_Read_Data (uint32_t StartPageAddress, uint32_t *RxBuf, uint16_t numberofwords);

#endif /* SRC_FLASH_H_ */
