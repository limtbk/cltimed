/* USER CODE BEGIN Header */
/**
  ******************************************************************************
  * @file           : main.c
  * @brief          : Main program body
  ******************************************************************************
  * @attention
  *
  * Copyright (c) 2023 STMicroelectronics.
  * All rights reserved.
  *
  * This software is licensed under terms that can be found in the LICENSE file
  * in the root directory of this software component.
  * If no LICENSE file comes with this software, it is provided AS-IS.
  *
  ******************************************************************************
  */
/* USER CODE END Header */
/* Includes ------------------------------------------------------------------*/
#include "main.h"

/* Private includes ----------------------------------------------------------*/
/* USER CODE BEGIN Includes */

#include "stdio.h"
#include "math.h"
#include "ds3231.h"
#include "timecl.h"
#include "ctype.h"
#include "flash.h"

/* USER CODE END Includes */

/* Private typedef -----------------------------------------------------------*/
/* USER CODE BEGIN PTD */

/* USER CODE END PTD */

/* Private define ------------------------------------------------------------*/
/* USER CODE BEGIN PD */

/* USER CODE END PD */

/* Private macro -------------------------------------------------------------*/
/* USER CODE BEGIN PM */

/* USER CODE END PM */

/* Private variables ---------------------------------------------------------*/
I2C_HandleTypeDef hi2c1;

UART_HandleTypeDef huart1;

/* USER CODE BEGIN PV */

uint8_t UART1_rxBuffer[24] = {0};
uint8_t UART1_expectToRead = 0;
uint8_t UART1_txBuffer[24] = {0};

/* USER CODE END PV */

/* Private function prototypes -----------------------------------------------*/
void SystemClock_Config(void);
static void MX_GPIO_Init(void);
static void MX_I2C1_Init(void);
static void MX_USART1_UART_Init(void);
/* USER CODE BEGIN PFP */
time_t LoadCorrection();
void SaveCorrection(time_t c);
/* USER CODE END PFP */

/* Private user code ---------------------------------------------------------*/
/* USER CODE BEGIN 0 */

/* USER CODE END 0 */

/**
  * @brief  The application entry point.
  * @retval int
  */
int main(void)
{
  /* USER CODE BEGIN 1 */

  /* USER CODE END 1 */

  /* MCU Configuration--------------------------------------------------------*/

  /* Reset of all peripherals, Initializes the Flash interface and the Systick. */
  HAL_Init();

  /* USER CODE BEGIN Init */

  /* USER CODE END Init */

  /* Configure the system clock */
  SystemClock_Config();

  /* USER CODE BEGIN SysInit */

  /* USER CODE END SysInit */

  /* Initialize all configured peripherals */
  MX_GPIO_Init();
  MX_I2C1_Init();
  MX_USART1_UART_Init();
  /* USER CODE BEGIN 2 */
  UART1_expectToRead = 1;
  HAL_UART_Receive_IT (&huart1, UART1_rxBuffer, UART1_expectToRead); // wait for first command symbol
  Ds3231_Init(&hi2c1);
  HAL_IncTick();
  time_t corr = LoadCorrection();
  if (corr == -1) { // first run
	  corr = 0;
  }
  SetT0(corr);
//  Set_Time(00, 00, 20, 7, 4, 6, 23);

  /* USER CODE END 2 */

  /* Infinite loop */
  /* USER CODE BEGIN WHILE */
  while (1)
  {
    /* USER CODE END WHILE */

    /* USER CODE BEGIN 3 */
	  // blink built-in LED to ensure that uC is not stuck
	  HAL_GPIO_WritePin(ONBOARD_LED_GPIO_Port, ONBOARD_LED_Pin, GPIO_PIN_RESET);
	  HAL_Delay(10);
	  HAL_GPIO_WritePin(ONBOARD_LED_GPIO_Port, ONBOARD_LED_Pin, GPIO_PIN_SET);
	  HAL_Delay(990);
  }
  /* USER CODE END 3 */
}

/**
  * @brief System Clock Configuration
  * @retval None
  */
void SystemClock_Config(void)
{
  RCC_OscInitTypeDef RCC_OscInitStruct = {0};
  RCC_ClkInitTypeDef RCC_ClkInitStruct = {0};

  /** Initializes the RCC Oscillators according to the specified parameters
  * in the RCC_OscInitTypeDef structure.
  */
  RCC_OscInitStruct.OscillatorType = RCC_OSCILLATORTYPE_HSE;
  RCC_OscInitStruct.HSEState = RCC_HSE_ON;
  RCC_OscInitStruct.HSEPredivValue = RCC_HSE_PREDIV_DIV1;
  RCC_OscInitStruct.HSIState = RCC_HSI_ON;
  RCC_OscInitStruct.PLL.PLLState = RCC_PLL_ON;
  RCC_OscInitStruct.PLL.PLLSource = RCC_PLLSOURCE_HSE;
  RCC_OscInitStruct.PLL.PLLMUL = RCC_PLL_MUL9;
  if (HAL_RCC_OscConfig(&RCC_OscInitStruct) != HAL_OK)
  {
    Error_Handler();
  }

  /** Initializes the CPU, AHB and APB buses clocks
  */
  RCC_ClkInitStruct.ClockType = RCC_CLOCKTYPE_HCLK|RCC_CLOCKTYPE_SYSCLK
                              |RCC_CLOCKTYPE_PCLK1|RCC_CLOCKTYPE_PCLK2;
  RCC_ClkInitStruct.SYSCLKSource = RCC_SYSCLKSOURCE_PLLCLK;
  RCC_ClkInitStruct.AHBCLKDivider = RCC_SYSCLK_DIV1;
  RCC_ClkInitStruct.APB1CLKDivider = RCC_HCLK_DIV2;
  RCC_ClkInitStruct.APB2CLKDivider = RCC_HCLK_DIV1;

  if (HAL_RCC_ClockConfig(&RCC_ClkInitStruct, FLASH_LATENCY_2) != HAL_OK)
  {
    Error_Handler();
  }
}

/**
  * @brief I2C1 Initialization Function
  * @param None
  * @retval None
  */
static void MX_I2C1_Init(void)
{

  /* USER CODE BEGIN I2C1_Init 0 */

  /* USER CODE END I2C1_Init 0 */

  /* USER CODE BEGIN I2C1_Init 1 */

  /* USER CODE END I2C1_Init 1 */
  hi2c1.Instance = I2C1;
  hi2c1.Init.ClockSpeed = 100000;
  hi2c1.Init.DutyCycle = I2C_DUTYCYCLE_2;
  hi2c1.Init.OwnAddress1 = 0;
  hi2c1.Init.AddressingMode = I2C_ADDRESSINGMODE_7BIT;
  hi2c1.Init.DualAddressMode = I2C_DUALADDRESS_DISABLE;
  hi2c1.Init.OwnAddress2 = 0;
  hi2c1.Init.GeneralCallMode = I2C_GENERALCALL_DISABLE;
  hi2c1.Init.NoStretchMode = I2C_NOSTRETCH_DISABLE;
  if (HAL_I2C_Init(&hi2c1) != HAL_OK)
  {
    Error_Handler();
  }
  /* USER CODE BEGIN I2C1_Init 2 */

  /* USER CODE END I2C1_Init 2 */

}

/**
  * @brief USART1 Initialization Function
  * @param None
  * @retval None
  */
static void MX_USART1_UART_Init(void)
{

  /* USER CODE BEGIN USART1_Init 0 */

  /* USER CODE END USART1_Init 0 */

  /* USER CODE BEGIN USART1_Init 1 */

  /* USER CODE END USART1_Init 1 */
  huart1.Instance = USART1;
  huart1.Init.BaudRate = 115200;
  huart1.Init.WordLength = UART_WORDLENGTH_8B;
  huart1.Init.StopBits = UART_STOPBITS_1;
  huart1.Init.Parity = UART_PARITY_NONE;
  huart1.Init.Mode = UART_MODE_TX_RX;
  huart1.Init.HwFlowCtl = UART_HWCONTROL_NONE;
  huart1.Init.OverSampling = UART_OVERSAMPLING_16;
  if (HAL_UART_Init(&huart1) != HAL_OK)
  {
    Error_Handler();
  }
  /* USER CODE BEGIN USART1_Init 2 */

  /* USER CODE END USART1_Init 2 */

}

/**
  * @brief GPIO Initialization Function
  * @param None
  * @retval None
  */
static void MX_GPIO_Init(void)
{
  GPIO_InitTypeDef GPIO_InitStruct = {0};
/* USER CODE BEGIN MX_GPIO_Init_1 */
/* USER CODE END MX_GPIO_Init_1 */

  /* GPIO Ports Clock Enable */
  __HAL_RCC_GPIOC_CLK_ENABLE();
  __HAL_RCC_GPIOD_CLK_ENABLE();
  __HAL_RCC_GPIOA_CLK_ENABLE();
  __HAL_RCC_GPIOB_CLK_ENABLE();

  /*Configure GPIO pin Output Level */
  HAL_GPIO_WritePin(ONBOARD_LED_GPIO_Port, ONBOARD_LED_Pin, GPIO_PIN_RESET);

  /*Configure GPIO pin : ONBOARD_LED_Pin */
  GPIO_InitStruct.Pin = ONBOARD_LED_Pin;
  GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
  GPIO_InitStruct.Pull = GPIO_NOPULL;
  GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
  HAL_GPIO_Init(ONBOARD_LED_GPIO_Port, &GPIO_InitStruct);

/* USER CODE BEGIN MX_GPIO_Init_2 */
/* USER CODE END MX_GPIO_Init_2 */
}

/* USER CODE BEGIN 4 */

void UART_printversion()
{
	sprintf((char *)UART1_txBuffer, "01.01\n\r");
	HAL_UART_Transmit(&huart1, UART1_txBuffer, 7, 100);
}

// Print to UART uint64_t time in hex
void UART_printhextime(time_t time)
{
	uint32_t lo = (uint32_t)(time & 0x00000000FFFFFFFF);
	uint32_t hi = (uint32_t)((time >> 32) & 0x00000000FFFFFFFF);
	sprintf((char *)UART1_txBuffer, "%08lX%08lX\n\r", hi, lo);
	HAL_UART_Transmit(&huart1, UART1_txBuffer, 18, 100);
}

// Print to UART human readable time
void UART_printhrtime(struct tm *time)
{
	sprintf ((char *)UART1_txBuffer, "%04d-%02d-%02d ", time->tm_year + 1900, time->tm_mon + 1, time->tm_mday);
	HAL_UART_Transmit(&huart1, UART1_txBuffer, 11, 100);
	sprintf ((char *)UART1_txBuffer, "%02d:%02d:%02d\n\r", time->tm_hour, time->tm_min, time->tm_sec);
	HAL_UART_Transmit(&huart1, UART1_txBuffer, 10, 100);
}


void HAL_UART_RxCpltCallback(UART_HandleTypeDef *huart)
{
	switch (toupper(UART1_rxBuffer[0])) {
		case 'V': // Hardware.firmware version
		{
			UART_printversion();
			UART1_expectToRead = 1;
			HAL_UART_Receive_IT(&huart1, UART1_rxBuffer, UART1_expectToRead);
			break;
		}
		case 'T': // Unix Earth time UTC in hex
		{
			struct tm time;
			Ds3231_Get_Time(&time);
			time_t unixtime = mktime(&time);
			UART_printhextime(unixtime);
			UART1_expectToRead = 1;
			HAL_UART_Receive_IT(&huart1, UART1_rxBuffer, UART1_expectToRead);
			break;
		}
		case 'U': // Unix CL time in hex
		{
			struct tm time;
			Ds3231_Get_Time(&time);
			time_t unixtime = mktime(&time);
			time_t lctime = Convert_Earth_To_CL(unixtime);
			UART_printhextime(lctime);
			UART1_expectToRead = 1;
			HAL_UART_Receive_IT(&huart1, UART1_rxBuffer, UART1_expectToRead);
			break;
		}
		case 'H': // Human readable Earth time UTC
		{
			struct tm time;
			Ds3231_Get_Time(&time);
			UART_printhrtime(&time);
			UART1_expectToRead = 1;
			HAL_UART_Receive_IT(&huart1, UART1_rxBuffer, UART1_expectToRead);
			break;
		}
		case 'G': //  Human readable CL time
		{
			struct tm time;
			Ds3231_Get_Time(&time);
			time_t unixtime = mktime(&time);
			time_t lctime = Convert_Earth_To_CL(unixtime);
			struct tm *cltime = gmtime(&lctime);
			UART_printhrtime(cltime);
			UART1_expectToRead = 1;
			HAL_UART_Receive_IT(&huart1, UART1_rxBuffer, UART1_expectToRead);
			break;
		}
		case 'I': // convert Earth time to CL time in hex
		{
			if (UART1_expectToRead == 1) {
				UART1_expectToRead = 16;
				HAL_UART_Receive_IT(&huart1, &(UART1_rxBuffer[1]), UART1_expectToRead);
			} else {
//				time_t unixtime = strtoull(&(UART1_rxBuffer[1]), NULL, 16);
				time_t unixtime = TimeFromHex(&(UART1_rxBuffer[1]));
				time_t lctime = Convert_Earth_To_CL(unixtime);
				UART_printhextime(lctime);
				UART1_expectToRead = 1;
				HAL_UART_Receive_IT(&huart1, UART1_rxBuffer, UART1_expectToRead);
			}
			break;
		}
		case 'J': // convert Earth time to CL time in hex
		{
			if (UART1_expectToRead == 1) {
				UART1_expectToRead = 16;
				HAL_UART_Receive_IT(&huart1, &(UART1_rxBuffer[1]), UART1_expectToRead);
			} else {
//				time_t lctime = strtoull(&(UART1_rxBuffer[1]), NULL, 16);
				time_t lctime = TimeFromHex(&(UART1_rxBuffer[1]));
				time_t unixtime = Convert_CL_To_Earth(lctime);
				UART_printhextime(unixtime);
				UART1_expectToRead = 1;
				HAL_UART_Receive_IT(&huart1, UART1_rxBuffer, UART1_expectToRead);
			}
			break;
		}
		case 'Z': // set zeropoint of CL time (in Earth UTC time)
		{
			if (UART1_expectToRead == 1) {
				UART1_expectToRead = 16;
				HAL_UART_Receive_IT(&huart1, &(UART1_rxBuffer[1]), UART1_expectToRead);
			} else {
				time_t t0 = TimeFromHex(&(UART1_rxBuffer[1]));
				SetT0(t0);
				SaveCorrection(t0);
				UART1_expectToRead = 1;
				HAL_UART_Receive_IT(&huart1, UART1_rxBuffer, UART1_expectToRead);
			}
			break;
		}
		case 'X': // get zeropoint
		{
			time_t t0 = GetT0();
			UART_printhextime(t0);
			UART1_expectToRead = 1;
			HAL_UART_Receive_IT(&huart1, UART1_rxBuffer, UART1_expectToRead);
			break;
		}
		case 'Y': // get time correction
		{
			uint8_t tc = (uint8_t)Ds3231_Get_TimeCorrection();
			sprintf((char *)UART1_txBuffer, "%02X\n\r", tc);
			HAL_UART_Transmit(&huart1, UART1_txBuffer, 4, 100);
			UART1_expectToRead = 1;
			HAL_UART_Receive_IT(&huart1, UART1_rxBuffer, UART1_expectToRead);
			break;
		}
		case 'W': // set time correction
		{
			if (UART1_expectToRead == 1) {
				UART1_expectToRead = 2;
				HAL_UART_Receive_IT(&huart1, &(UART1_rxBuffer[1]), UART1_expectToRead);
			} else {
				int8_t tc = (uint8_t)strtol(&(UART1_rxBuffer[1]), NULL, 16);
				Ds3231_Set_TimeCorrection(tc);
				UART1_expectToRead = 1;
				HAL_UART_Receive_IT(&huart1, UART1_rxBuffer, UART1_expectToRead);
			}
			break;
		}

		case 'S': // set Earth UTC time, CL time will be affected
		{
			if (UART1_expectToRead == 1) {
				UART1_expectToRead = 16;
				HAL_UART_Receive_IT(&huart1, &(UART1_rxBuffer[1]), UART1_expectToRead);
			} else {
				time_t unixtime = TimeFromHex(&(UART1_rxBuffer[1]));
				struct tm *time = gmtime(&unixtime);
				Ds3231_Set_Time(time);
				UART1_expectToRead = 1;
				HAL_UART_Receive_IT(&huart1, UART1_rxBuffer, UART1_expectToRead);
			}
			break;
		}
		case 'Q': // set CL time, Earth UTC time will be affected
		{
			if (UART1_expectToRead == 1) {
				UART1_expectToRead = 16;
				HAL_UART_Receive_IT(&huart1, &(UART1_rxBuffer[1]), UART1_expectToRead);
			} else {
				time_t lctime = TimeFromHex(&(UART1_rxBuffer[1]));
				time_t unixtime = Convert_CL_To_Earth(lctime);
				struct tm *time = gmtime(&unixtime);
				Ds3231_Set_Time(time);
				UART1_expectToRead = 1;
				HAL_UART_Receive_IT(&huart1, UART1_rxBuffer, UART1_expectToRead);
			}
			break;
		}

		default:
		{
			UART1_expectToRead = 1;
			HAL_UART_Receive_IT(&huart1, UART1_rxBuffer, UART1_expectToRead);
			break;
		}
	}

//	HAL_UART_Receive_IT (&huart1, UART1_rxBuffer, UART1_expectToRead); // wait for next part
//    HAL_UART_Transmit(&huart1, UART1_rxBuffer, 8, 100);
}

time_t LoadCorrection()
{
	int a = sizeof(time_t);
	time_t correction = a;
	Flash_Read_Data(0x0801fc00 , (uint32_t *)&correction, 2);
	return correction;
}

void SaveCorrection(time_t c)
{
	Flash_Write_Data(0x0801fc00 , (uint32_t *)&c, 2);
}

/* USER CODE END 4 */

/**
  * @brief  This function is executed in case of error occurrence.
  * @retval None
  */
void Error_Handler(void)
{
  /* USER CODE BEGIN Error_Handler_Debug */
  /* User can add his own implementation to report the HAL error return state */
  __disable_irq();
  while (1)
  {
  }
  /* USER CODE END Error_Handler_Debug */
}

#ifdef  USE_FULL_ASSERT
/**
  * @brief  Reports the name of the source file and the source line number
  *         where the assert_param error has occurred.
  * @param  file: pointer to the source file name
  * @param  line: assert_param error line source number
  * @retval None
  */
void assert_failed(uint8_t *file, uint32_t line)
{
  /* USER CODE BEGIN 6 */
  /* User can add his own implementation to report the file name and line number,
     ex: printf("Wrong parameters value: file %s on line %d\r\n", file, line) */
  /* USER CODE END 6 */
}
#endif /* USE_FULL_ASSERT */
