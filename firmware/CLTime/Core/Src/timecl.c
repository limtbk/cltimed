/*
 * timecl.c
 *
 *  Created on: Jun 5, 2023
 *      Author: lim
 */

#include "timecl.h"
#include "math.h"
#include "ctype.h"

double cost[] = {1.0, 0.9951847266721969, 0.9807852804032304, 0.9569403357322088, 0.9238795325112867, 0.881921264348355, 0.8314696123025452, 0.773010453362737, 0.7071067811865476, 0.6343932841636456, 0.5555702330196023, 0.4713967368259978, 0.38268343236508984, 0.2902846772544623, 0.1950903220161283, 0.09801714032956077, 6.123233995736766e-17, -0.09801714032956066, -0.1950903220161282, -0.2902846772544622, -0.3826834323650897, -0.4713967368259977, -0.555570233019602, -0.6343932841636453, -0.7071067811865475, -0.773010453362737, -0.8314696123025453, -0.8819212643483549, -0.9238795325112867, -0.9569403357322088, -0.9807852804032304, -0.9951847266721968, -1.0, -0.9951847266721969, -0.9807852804032304, -0.9569403357322089, -0.9238795325112867, -0.881921264348355, -0.8314696123025455, -0.7730104533627371, -0.7071067811865477, -0.6343932841636459, -0.5555702330196022, -0.47139673682599786, -0.38268343236509034, -0.2902846772544624, -0.19509032201612866, -0.09801714032956045, -1.8369701987210297e-16, 0.09801714032956009, 0.19509032201612828, 0.2902846772544621, 0.38268343236509, 0.4713967368259976, 0.5555702330196018, 0.6343932841636456, 0.7071067811865475, 0.7730104533627367, 0.8314696123025452, 0.8819212643483549, 0.9238795325112865, 0.9569403357322088, 0.9807852804032303, 0.9951847266721969};
double cosdx = 2 * M_PI;
int cosn = 64;
double revt[] = {0.0, -0.0022964052455830284, -0.008770693663814465, -0.018875582582780756, -0.0321486164199248, -0.04819607505205372, -0.06668007007514987, -0.0873082399318598, -0.10982551244868195, -0.13400749368735096, -0.15965513080760116, -0.18659037258616112, -0.21465261245425069, -0.24369574680827408, -0.27358571825135863, -0.3041984416494012, -0.3354180323847793, -0.3671352725291344, -0.39924626304235, -0.4316512194379168, -0.4642533753115458, -0.49695796321954466, -0.5296712459434676, -0.5622995734605171, -0.5947484420782362, -0.6269215322982079, -0.6587197010179662, -0.6900399016246621, -0.7207740022316349, -0.7508074675354731, -0.7800178631836747, -0.8082731327095023, -0.835429585319696, -0.8613295173122729, -0.8857983694577507, -0.9086412958432982, -0.9296389845570223, -0.9485425249141715, -0.9650670573584175, -0.978883868951955, -0.9896105105282823, -0.9967984198780125, -0.9999174671989273, -0.998336870100703, -0.9913022412735893, -0.9779095690851929, -0.9570796827090876, -0.9275433800828998, -0.8878622115708538, -0.8365398291043398, -0.7723276948449735, -0.6948669598316765, -0.6056940814199139, -0.5091251346148583, -0.4118768230438791, -0.3209138844471057, -0.24108444470281049, -0.17439489751792853, -0.12075509877352242, -0.07902119742307226, -0.047711688083061754, -0.025364862809632932, -0.010676756359279848, -0.0025333912020273604};
double revdx = 4 * M_PI;
int revn = 64;

time_t t0 = 0; //Unix UTC earth time when CL time is 0

// Quadratic approximation of periodical functions
// xi  - argument
// dx - period
// N - number of samples
// yt - array of samples

double ApproxQ(double xi, double dx, int N, double *yt)
{
    double x = xi - floor(xi / dx) * dx;
    int nx = (int)((x / dx) * N);

    double x0 = dx * nx / N;
    double x1 = dx * (nx + 1) / N;
	double x2 = dx * (nx + 2) / N;

	double y0 = yt[nx % N];
	double y1 = yt[(nx + 1) % N];
	double y2 = yt[(nx + 2) % N];

	double L0x = (x - x1) * (x - x2) / ((x0 - x1) * (x0 - x2));
	double L1x = (x - x0) * (x - x2) / ((x1 - x0) * (x1 - x2));
	double L2x = (x - x0) * (x - x1) / ((x2 - x0) * (x2 - x1));

	double y = y0 * L0x + y1 * L1x + y2 * L2x;

	return y;
}

// By definition, time on CL relative to time on Earth depends on formula dTcl/dTe = sin(Te)+2
// So the whole time will be calculated in formula Tcl = Integral(0, Te, (sin(Te)+2)*dTe)
// After calculations, we got Tcl = 2*Te - cos(Te) + 1
// Unfortunately, reverse function could not be represented analytically, so it's implemented
// with quadratic approximation. And becauseapproximation is already implemented, it could be
// used for cos() as well

time_t Convert_CL_To_Earth(time_t clTime)
{
	double t = clTime;
	double eat = t / 2 + ApproxQ(t, revdx, revn, revt) + 1; // reverse function for 2 * t - cos(t) + 1
	time_t eaTime = eat + t0;
	return eaTime;
}

time_t Convert_Earth_To_CL(time_t eaTime)
{
	double t = eaTime - t0;
	double clt = 2 * t - ApproxQ(t, cosdx, cosn, cost) + 1; // 2 * t - cos(t) + 1;
	time_t clTime = clt;
	return clTime;
}

// Getter and setter for T0 - UTC Earth timestamp when 0 time on CL started
time_t GetT0()
{
	return t0;
}

void SetT0(time_t t)
{
	t0 = t;
}

// strtol(), strtoull() are not work with hex numbers, where 31-th digit is 1 -
// it interpret it as negative number. So I had to implement my own converter
time_t TimeFromHex(uint8_t s[])
{
	time_t result = 0;

	for (int i = 0; i < 16; i++) {
		char c = toupper(s[i]);
		if (c>='0' && c<='9') {
			result = result << 4;
			result = result + (c - '0');
		} else if (c>='A' && c<='F') {
			result = result << 4;
			result = result + (c - 'A' + 10);
		} else {
			break;
		}
	}

	return result;
}
