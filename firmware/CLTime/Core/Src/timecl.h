/*
 * timecl.h
 *
 *  Created on: Jun 5, 2023
 *      Author: lim
 */

#ifndef SRC_TIMECL_H_
#define SRC_TIMECL_H_

#include "time.h"

time_t Convert_CL_To_Earth(time_t clTime);
time_t Convert_Earth_To_CL(time_t eaTime);

time_t GetT0();
void SetT0(time_t t);

time_t TimeFromHex(uint8_t s[]);

#endif /* SRC_TIMECL_H_ */
