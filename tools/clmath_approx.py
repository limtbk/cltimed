import matplotlib.pyplot as plt
import numpy as np

def F(x):
    y = 2 * x - np.cos(x) + 1
    return y

x0hr = np.linspace(0, np.pi * 2, 1024)
y0hr = F(x0hr)

x0ihr = y0hr
y0ihr = x0hr

y0idhr = y0ihr - x0ihr / 2
x0idhr = x0ihr


x0 = np.linspace(0, np.pi * 2, 1024)
y0 = F(x0)

x0i = y0
y0i = x0

y0id = y0i - x0i/2
x0id = x0i

def approxF(x, xa, ya):
    for x0, x1, y0, y1 in zip(xa[:-1], xa[1:], ya[:-1], ya[1:]):
        if x0 <= x and x <= x1:
            # y = ((x - x0) / (x1 - x0)) * (y1 - y0) + y0
            y = y0 * (x1 - x) / (x1 - x0) + y1 * (x - x0) / (x1 - x0)
            return y

def approxQF(x, xa, ya):
    for x0, x1, x2, y0, y1, y2 in zip(xa[:-2], xa[1:-1], xa[2:], ya[:-2], ya[1:-1], ya[2:]):
        if x0 <= x and x <= x2:
            L0x = (x - x1) * (x - x2) / ((x0 - x1) * (x0 - x2))
            L1x = (x - x0) * (x - x2) / ((x1 - x0) * (x1 - x2))
            L2x = (x - x0) * (x - x1) / ((x2 - x0) * (x2 - x1))
            # y = ((x - x0) / (x1 - x0)) * (y1 - y0) + y0
            y = y0 * L0x + y1 * L1x + y2 * L2x
            return y


xap = np.linspace(0, np.pi*4, 1024)
yap = []
for xxap in xap:
    yap.append(approxF(xxap, x0id, y0id))

xqap = np.linspace(0, np.pi*4, 1024)
yqap = []
for xxqap in xqap:
    yqap.append(approxQF(xxqap, x0id, y0id))

xtbl = np.linspace(0, np.pi*4, 65)
ytbl = []
for xxtbl in xtbl:
    ytbl.append(approxQF(xxtbl, x0id, y0id))

for xx, yy in zip(xtbl, ytbl):
    print(xx, yy)

xcos = np.linspace(0, np.pi*2, 65)
ycos = np.cos(xcos)

for xx, yy in zip(xcos, ycos):
    print(xx, yy)


# x_0 = np.linspace(0,np.pi*2,100)
# #y_0 = np.linspace(-2.5,3,1000)
# y_f = 2 * x_0 - np.cos(x_0) + 1
# xx_0 = x_0 - y_f / 2

plt.figure()
# plt.plot(y_0, x_hat, 'k-', linewidth = 2, label = 'Taylor Approximation $\\widehat{f}^{-1}(y)$')
# plt.plot(y_f, xx_0, 'k-', linewidth = 2, label = 'Actual $f^{-1}(y)$')
# plt.plot(x0idhr, y0idhr, 'k-', linewidth = 2, label = 'Actual $f^{-1}(y)$')
# plt.plot(xap, yap, '-.', linewidth = 2, label = 'Actual $f^{-1}(y)$')
plt.plot(xqap, yqap, '--', linewidth = 2, label = 'Actual $f^{-1}(y)$')
plt.xlabel("$y$", fontsize = 16)
plt.ylabel("$x = f^{-1}(y)$", fontsize = 16)
plt.title("Addition to $y=x/21$",fontsize = 20)
plt.legend(fontsize = 12)
plt.grid(which = 'both')
plt.show()
